This project will use Telldus Local API to control the temperature in multiple rooms individually.

This is the client repo. For server see [eirikandre/telldus-thermostat-client](https://github.com/eirikandre/telldus-thermostat-client)

For documentation, see https://eirikandre.github.io/telldus-thermostat/

# Build

Standard Spring Boot maven project. Can be built with
```
mvn install
```

MongoDB is required for backend. That is easiest to get by using docker-compose:

```
version: '3'
   
   services:
     database:
       image: mongo
       volumes:
         -  mongodata:/data/db
       ports:
         - 27017:27017
   volumes:
     mongodata:

```

The database can be started with 


```
docker-compose up -d
```
