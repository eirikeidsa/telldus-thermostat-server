package no.eidsa.termostat.temperature;

import no.eidsa.termostat.room.Room;
import no.eidsa.termostat.room.RoomService;
import no.eidsa.termostat.telldus.TelldusService;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class TemperatureControllerServiceTest {

    private TemperatureControllerService temperatureControllerService;

    @Before
    public void setup() {
        TelldusService telldusService = mock(TelldusService.class);

        RoomService roomService = mock(RoomService.class);

        temperatureControllerService = new TemperatureControllerService(roomService, telldusService);

    }

    @Test
    public void testThatOvenIsTurnedOffWhenRoomTempIs21() throws Exception {
        Room room = new Room();
        room.setCurrentTemperature(21D);
        room.setFlexTemperature(0.7D);
        room.setDesiredTemperature(20D);

        assertEquals(OvenDeciderStatus.OFF, temperatureControllerService.decideOvenAction(room));
    }

    @Test
    public void testThatOvenIsTurnedOffWhenRoomTempIs21_7() throws Exception {
        assertEquals(OvenDeciderStatus.OFF, temperatureControllerService.decideOvenAction(getRoom(21.7)));
    }

    @Test
    public void testThatOvenIsTurnedOffWhenRoomTempIs22() throws Exception {
        assertEquals(OvenDeciderStatus.OFF, temperatureControllerService.decideOvenAction(getRoom(22D)));
    }

    @Test
    public void testThatOvenIsTurnedOffWhenRoomTempIs22_1() throws Exception {
        assertEquals(OvenDeciderStatus.OFF, temperatureControllerService.decideOvenAction(getRoom(22.1D)));
    }

    @Test
    public void testThatOvenIsTurnedOffWhenRoomTempIs23() throws Exception {
        assertEquals(OvenDeciderStatus.OFF, temperatureControllerService.decideOvenAction(getRoom(23D)));
    }

    @Test
    public void testThatOvenIsTurnedOnWhenRoomTempIs19() throws Exception {
        assertEquals(OvenDeciderStatus.ON, temperatureControllerService.decideOvenAction(getRoom(19D)));
    }

    @Test
    public void testThatOvenDoesNotChangeWhenRoomTempIs20_2() throws Exception {
        assertEquals(OvenDeciderStatus.NO_CHANGE, temperatureControllerService.decideOvenAction(getRoom(20.2D)));
    }

    @Test
    public void testThatOvenDoesNotChangeWhenRoomTempIs19_5() throws Exception {
        assertEquals(OvenDeciderStatus.NO_CHANGE, temperatureControllerService.decideOvenAction(getRoom(19.5D)));
    }

    private Room getRoom(Double currentTemperature) {
        Room room = new Room();
        room.setCurrentTemperature(currentTemperature);
        room.setFlexTemperature(0.7D);
        room.setDesiredTemperature(20D);
        return room;
    }

}