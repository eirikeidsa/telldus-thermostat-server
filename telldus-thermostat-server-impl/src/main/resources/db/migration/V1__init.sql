create table room (
  id varchar (255)
    primary key,
  name varchar (255),
  desired_temperature double null,
  current_temperature double null,
  flex_temperature double null,
  sensor_id varchar (255),
  device_id varchar (255)
);

create table cron_temperature (
  id varchar (255) primary key,
  room varchar (244),
  temperature double,
  active bit,
  cron varchar (255)

);