package no.eidsa.termostat.room;

import org.springframework.stereotype.Component;

@Component
public class PrometheusService {

    private final RoomService roomService;

    private final static String template = "# HELP thermostat_room_#name#_#type#  \n" +
            "# TYPE thermostat_room_#name#_#type# gauge\n" +
            "thermostat_room_#name#_#type# #value#\n";

    public PrometheusService(RoomService roomService) {
        this.roomService = roomService;
    }


    public String generate() {
        StringBuilder stringBuilder = new StringBuilder();

        roomService.getRooms().forEach(room -> generateForRoom(stringBuilder, room));

        return stringBuilder.toString();
    }

    private void generateForRoom(StringBuilder stringBuilder, Room room) {
        stringBuilder.append(generateItem(room.getName(), "temp_current", String.valueOf(room.getCurrentTemperature())));
        stringBuilder.append(generateItem(room.getName(), "temp_desired", String.valueOf(room.getDesiredTemperature())));
        stringBuilder.append(generateItem(room.getName(), "device_status", String.valueOf(room.getDeviceStatus())));
        stringBuilder.append(generateItem(room.getName(), "device_kw", String.valueOf(room.getDeviceKw())));
        stringBuilder.append(generateKw(room.getName(), "device_status_kw", room.getDeviceStatus(), room.getDeviceKw()));
    }

    private static String generateKw(String name, String type, Integer statusValue, Double kwValue) {
        String displayValue;

        if (statusValue == null || statusValue == 0) {
            displayValue = "0";
        } else {
            displayValue = String.valueOf(kwValue);
        }

        return template.replaceAll("#name#", name.replace(" ", "").toLowerCase()).replaceAll("#type#", type).replaceAll("#value#", displayValue);
    }

    private static String generateItem(String name, String type, String value) {

        String newValue;

        if ("null".equals(value)) {
            newValue = "0";
        } else {
            newValue = value;
        }

        return template.replaceAll("#name#", name.replace(" ", "").toLowerCase()).replaceAll("#type#", type).replaceAll("#value#", newValue);
    }

}
