package no.eidsa.termostat.room;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import no.eidsa.termostat.temperature.OvenDeciderStatus;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Room {

    @Id
    private String id;
    private String name;
    private Double desiredTemperature;
    private Double currentTemperature;
    private Double flexTemperature;

    private String sensorId;
    private String deviceId;

    private Integer deviceStatus;
    private Double deviceKw;

}
