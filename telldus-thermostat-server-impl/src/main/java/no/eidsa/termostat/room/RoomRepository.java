package no.eidsa.termostat.room;

import org.springframework.data.repository.CrudRepository;

public interface RoomRepository  extends CrudRepository<Room, String> {


}
