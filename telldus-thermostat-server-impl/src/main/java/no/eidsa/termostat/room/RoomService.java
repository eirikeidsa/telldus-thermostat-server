package no.eidsa.termostat.room;

import no.eidsa.termostat.exception.BaseException;
import no.eidsa.termostat.telldus.TelldusServiceApi;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class RoomService {

    private final RoomRepository roomRepository;

    private final TelldusServiceApi telldusService;

    public RoomService(RoomRepository roomRepository, TelldusServiceApi telldusService) {
        this.roomRepository = roomRepository;
        this.telldusService = telldusService;
        Assert.notNull(telldusService, "telldusService should not be null");
        Assert.notNull(roomRepository, "roomRepository should not be null");
    }

    public Room getRoom(String roomId) {
        return roomRepository.findById(roomId).orElseThrow(() -> new BaseException("Not able to find any room with id " + roomId));
    }

    public List<Room> getRooms() {
        List<Room> rooms = new ArrayList<>();
        roomRepository.findAll().forEach(rooms::add);

        return rooms;
    }

    public Room saveRoom(Room room) {
        Double currentTemperature = telldusService.getCurrentTemperature(room.getSensorId());
        room.setCurrentTemperature(currentTemperature);

        if(room.getId() == null) {
            room.setId(UUID.randomUUID().toString());
        }

        return roomRepository.save(room);
    }

    public Room setTemperature(String roomId, Double temperature) {
        Room one = roomRepository.findById(roomId).orElseThrow(() -> new BaseException("Not able to find any room with id " + roomId));
        one.setDesiredTemperature(temperature);
        return roomRepository.save(one);
    }

    public void deleteRoom(String roomId) {
        roomRepository.deleteById(roomId);
    }

    public void deleteAllRooms() {
        roomRepository.deleteAll();
    }

    public void saveRooms(List<Room> rooms) {
        roomRepository.saveAll(rooms);
    }
}
