package no.eidsa.termostat.room.rest;

import lombok.extern.slf4j.Slf4j;
import no.eidsa.termostat.ConvertUtilities;
import no.eidsa.termostat.api.RoomsApi;
import no.eidsa.termostat.api.domain.RoomListTO;
import no.eidsa.termostat.api.domain.RoomTO;
import no.eidsa.termostat.api.domain.TemperatureStatusTO;
import no.eidsa.termostat.exception.BaseException;
import no.eidsa.termostat.room.Room;
import no.eidsa.termostat.room.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.validation.Valid;
import java.math.BigDecimal;

@Component
@Slf4j
public class RoomResource implements RoomsApi {

    private final RoomService roomService;

    @Autowired
    public RoomResource(RoomService roomService) {
        Assert.notNull(roomService, "roomService should not be null");
        this.roomService = roomService;
    }

    @Override
    public void deleteRoom(String roomId) throws Exception {
        roomService.deleteRoom(roomId);
    }

    @Override
    public RoomListTO getRoomList() throws Exception {
        RoomListTO to = new RoomListTO();
        roomService.getRooms().forEach(room -> to.getRooms().add(ConvertUtilities.convert(room)));
        return to;
    }

    @Override
    public RoomTO getRoom(String roomId) throws Exception {
        Room room = roomService.getRoom(roomId);
        return ConvertUtilities.convert(room);
    }

    @Override
    public TemperatureStatusTO getTemperatureStatus(String roomId) throws Exception {
        TemperatureStatusTO status = new TemperatureStatusTO();
        Room room = roomService.getRoom(roomId);
        status.setDesiredTemperature(BigDecimal.valueOf(room.getDesiredTemperature()));
        status.setCurrentTemperature(BigDecimal.valueOf(room.getCurrentTemperature()));

        return status;
    }

    @Override
    public RoomTO postRoom(@Valid RoomTO body) throws Exception {
        Room room = roomService.saveRoom(ConvertUtilities.convert(body));
        return ConvertUtilities.convert(room);
    }



    @Override
    public RoomTO putRoom(String roomId, RoomTO roomTO) throws Exception {
        return ConvertUtilities.convert(roomService.saveRoom(ConvertUtilities.convert(roomTO)));
    }

    @Override
    public TemperatureStatusTO putTemperature(String roomId, BigDecimal temperature) throws Exception {

        Room room = roomService.setTemperature(roomId, temperature.doubleValue());

        if (room != null) {
            TemperatureStatusTO temperatureStatus = new TemperatureStatusTO();
            temperatureStatus.setDesiredTemperature(BigDecimal.valueOf(room.getDesiredTemperature()));
            temperatureStatus.setCurrentTemperature(BigDecimal.valueOf(room.getCurrentTemperature()));
            return temperatureStatus;

        } else {
            throw new BaseException("Did not find the room");
        }

    }



}
