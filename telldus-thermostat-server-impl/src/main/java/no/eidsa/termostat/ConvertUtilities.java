package no.eidsa.termostat;

import no.eidsa.termostat.api.domain.CronTemperatureTO;
import no.eidsa.termostat.api.domain.RoomTO;
import no.eidsa.termostat.cron.CronTemperature;
import no.eidsa.termostat.room.Room;

import java.math.BigDecimal;

public final class ConvertUtilities {


    private ConvertUtilities() {
    }

    public static CronTemperature convert(CronTemperatureTO to) {
        CronTemperature cronTemperature = new CronTemperature();
        cronTemperature.setActive(to.isActive());
        cronTemperature.setCron(to.getCron());
        cronTemperature.setId(to.getId());
        cronTemperature.setRoom(to.getRoom());
        cronTemperature.setTemperature(to.getTemperature().doubleValue());

        return cronTemperature;
    }

    public static CronTemperatureTO convert(CronTemperature cronTemperature) {
        CronTemperatureTO to = new CronTemperatureTO();

        to.setActive(cronTemperature.getActive());
        to.setCron(cronTemperature.getCron());
        to.setId(cronTemperature.getId());
        to.setRoom(cronTemperature.getRoom());
        to.setTemperature(BigDecimal.valueOf(cronTemperature.getTemperature()));

        return to;
    }

    public static RoomTO convert(Room room) {
        RoomTO to = new RoomTO();
        to.setSensorId(room.getSensorId());
        to.setName(room.getName());
        to.setId(room.getId());
        to.setDeviceId(room.getDeviceId());

        if (room.getCurrentTemperature() != null) {
            to.setCurrentTemperature(BigDecimal.valueOf(room.getCurrentTemperature()));
        }

        if (room.getDesiredTemperature() != null) {
            to.setDesiredTemperature(BigDecimal.valueOf(room.getDesiredTemperature()));
        }

        if (room.getFlexTemperature() != null) {
            to.setFlexTemperature(BigDecimal.valueOf(room.getFlexTemperature()));
        }

        if (room.getDeviceKw() != null) {
            to.setDeviceEffect(BigDecimal.valueOf(room.getDeviceKw()));
        }


        return to;
    }

    public static Room convert(RoomTO body) {
        Room room = new Room();

        room.setCurrentTemperature(convertToDouble(body.getCurrentTemperature()));
        room.setDesiredTemperature((convertToDouble(body.getDesiredTemperature())));
        room.setDeviceKw(convertToDouble(body.getDeviceEffect()));
        room.setDeviceStatus(body.getDeviceStatus());

        room.setDeviceId(body.getDeviceId());
        room.setFlexTemperature(convertToDouble(body.getFlexTemperature()));
        room.setId(body.getId());
        room.setName(body.getName());
        room.setSensorId(body.getSensorId());


        return room;
    }

    private static Double convertToDouble(BigDecimal bigDecimal) {
        if (bigDecimal == null) {
            return null;
        } else {
            return bigDecimal.doubleValue();
        }
    }

}
