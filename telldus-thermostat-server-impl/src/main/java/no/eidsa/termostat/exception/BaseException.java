package no.eidsa.termostat.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BaseException extends RuntimeException {

    public BaseException(String message) {
        super(message);
        log.error(message);
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
        log.error(message, cause);
    }
}