package no.eidsa.termostat.configuration;

import no.eidsa.termostat.cron.rest.CronResource;
import no.eidsa.termostat.export.ImportExportResource;
import no.eidsa.termostat.metrics.Prometheus;
import no.eidsa.termostat.room.rest.RoomResource;
import no.eidsa.termostat.telldus.rest.TelldusResource;
import no.eidsa.termostat.time.TimeResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.ApplicationPath;

@Configuration
@ApplicationPath("/api")
public class RestApplication extends ResourceConfig {

    public RestApplication() {
        register(CorsFilter.class);
        register(CronResource.class);
        register(ImportExportResource.class);
        register(RoomResource.class);
        register(TelldusResource.class);
        register(TimeResource.class);
        register(Prometheus.class);
    }


}
