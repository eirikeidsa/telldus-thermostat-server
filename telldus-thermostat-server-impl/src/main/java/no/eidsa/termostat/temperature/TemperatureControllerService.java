package no.eidsa.termostat.temperature;

import no.eidsa.termostat.room.Room;
import no.eidsa.termostat.room.RoomService;
import no.eidsa.termostat.telldus.TelldusServiceApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class TemperatureControllerService {

    private static final Logger logger = LoggerFactory.getLogger(TemperatureControllerService.class);

    private final TelldusServiceApi telldusService;

    private final RoomService roomService;

    @Autowired
    public TemperatureControllerService(RoomService roomService, TelldusServiceApi telldusService) {
        Assert.notNull(telldusService, "telldusService should not be null");
        Assert.notNull(roomService, "roomService should not be null");

        this.telldusService = telldusService;
        this.roomService = roomService;
    }

    @Scheduled(fixedRate = 60000)
    public void verifyTemperature() {

        for (Room room : roomService.getRooms()) {
            Double currentTemperature = telldusService.getCurrentTemperature(room.getSensorId());
            room.setCurrentTemperature(currentTemperature);
            OvenDeciderStatus ovenDeciderStatus = decideOvenAction(room);
            if (ovenDeciderStatus == OvenDeciderStatus.ON) {
                room.setDeviceStatus(1);
            } else if (ovenDeciderStatus == OvenDeciderStatus.OFF) {
                room.setDeviceStatus(0);
            }
            roomService.saveRoom(room);

            logger.debug("{} temp is now {}. Oven command: {}", room.getName(), currentTemperature, ovenDeciderStatus);
        }
    }

    OvenDeciderStatus decideOvenAction(Room room) {
        OvenDeciderStatus status = OvenDeciderStatus.NO_CHANGE;

        if (currentTemperatureTooHigh(room)) {
            telldusService.turnDeviceOff(room.getDeviceId());
            status = OvenDeciderStatus.OFF;
        } else if (currentTemperatureTooLow(room)) {
            telldusService.turnDeviceOn(room.getDeviceId());
            status = OvenDeciderStatus.ON;
        }

        return status;
    }

    private boolean currentTemperatureTooLow(Room room) {
        return room.getCurrentTemperature() < room.getDesiredTemperature() - room.getFlexTemperature();
    }

    private boolean currentTemperatureTooHigh(Room room) {
        return room.getCurrentTemperature() > room.getDesiredTemperature() + room.getFlexTemperature();
    }

}
