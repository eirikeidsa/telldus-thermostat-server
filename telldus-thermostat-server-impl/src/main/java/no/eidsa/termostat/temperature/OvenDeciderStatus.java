package no.eidsa.termostat.temperature;

/**
 * Used to test the logic inside the TemperatureControllerService
 */
public enum OvenDeciderStatus {

    NO_CHANGE,
    OFF,
    ON
}
