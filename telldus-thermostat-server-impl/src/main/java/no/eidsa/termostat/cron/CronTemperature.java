package no.eidsa.termostat.cron;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor
public class CronTemperature {

    @Id
    private String id;
    private String room;
    private String cron;
    private Double temperature;
    private Boolean active = false;

}
