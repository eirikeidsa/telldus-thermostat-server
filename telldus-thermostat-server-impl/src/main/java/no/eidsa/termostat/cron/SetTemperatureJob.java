package no.eidsa.termostat.cron;

import lombok.extern.slf4j.Slf4j;
import no.eidsa.termostat.room.RoomService;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@Slf4j
public class SetTemperatureJob implements Job {

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobDataMap mergedJobDataMap = jobExecutionContext.getMergedJobDataMap();

        RoomService roomService = (RoomService) mergedJobDataMap.get(QuartzService.JOB_MAP_ROOM_SERVICE);
        CronTemperature cronTemperature = (CronTemperature) mergedJobDataMap.get(QuartzService.JOB_MAP_CRON_TEMPERATURE);
        roomService.setTemperature(cronTemperature.getRoom(), cronTemperature.getTemperature());

        log.info("Temperature set in QuartzJob to {}", cronTemperature);
    }
}
