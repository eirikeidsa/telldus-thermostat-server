package no.eidsa.termostat.cron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class CronService {

    private final QuartzService quartzService;

    private final CronRepository cronRepository;

    @Autowired
    public CronService(QuartzService quartzService, CronRepository cronRepository) {
        this.quartzService = quartzService;
        this.cronRepository = cronRepository;
        Assert.notNull(cronRepository, "cronRepository should not be null");
        Assert.notNull(quartzService, "quartzService should not be null");
    }

    public void updateCronJobs(List<CronTemperature> crons) {

        crons.forEach(this::setIdIfMissing);

        deleteAll();
        cronRepository.saveAll(crons);
        quartzService.schedule();
    }

    private void setIdIfMissing(CronTemperature cronTemperature) {
        if(cronTemperature.getId() == null) {
            cronTemperature.setId(UUID.randomUUID().toString());
        }

    }

    public List<CronTemperature> getCronJobs() {
        List<CronTemperature> crons = new ArrayList<>();

        cronRepository.findAll().forEach(crons::add);

        return crons;
    }

    public void deleteAll() {
        cronRepository.deleteAll();
        quartzService.deleteJobs();
    }
}
