package no.eidsa.termostat.cron;

import lombok.extern.slf4j.Slf4j;
import no.eidsa.termostat.exception.BaseException;
import no.eidsa.termostat.room.RoomService;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.text.ParseException;

import static org.quartz.CronScheduleBuilder.cronScheduleNonvalidatedExpression;
import static org.quartz.TriggerBuilder.newTrigger;

@Service
@Slf4j
public class QuartzService {

    static final String JOB_MAP_CRON_TEMPERATURE = "cronTemperature";
    static final String JOB_MAP_ROOM_SERVICE = "roomService";

    private final CronRepository cronRepository;
    private final RoomService roomService;

    private Scheduler scheduler;

    private JobDetail job;
    private JobKey jobKey = JobKey.jobKey("tempJob");

    @Autowired
    public QuartzService(RoomService roomService, CronRepository cronRepository) {
        this.roomService = roomService;
        this.cronRepository = cronRepository;
        try {
            scheduler = StdSchedulerFactory.getDefaultScheduler();
            scheduler.start();

            job = JobBuilder.newJob(SetTemperatureJob.class).withIdentity(jobKey).storeDurably().build();
        } catch (SchedulerException e) {
            log.error("Failed to start the job", e);
        }
    }

    @PostConstruct
    public void schedule() {
        deleteJobs();

        try {
            scheduler.addJob(job, true);

        } catch (SchedulerException e) {
            throw new BaseException("Failed to schedule", e);
        }

        cronRepository.findAll().forEach(this::createScheduler);

    }

    private void createScheduler(CronTemperature time) {
        try {

            if (time.getActive()) {
                JobDataMap jobData = new JobDataMap();
                jobData.put(JOB_MAP_ROOM_SERVICE, roomService);
                jobData.put(JOB_MAP_CRON_TEMPERATURE, time);

                CronTrigger trigger = newTrigger()
                        .withIdentity("trigger" + time.hashCode(), "group" + time.hashCode())
                        .withSchedule(cronScheduleNonvalidatedExpression(time.getCron()))
                        .usingJobData(jobData)
                        .forJob(jobKey)
                        .build();

                scheduler.scheduleJob(trigger);
                log.info("Job scheduled for {}", time.getCron());
            } else {
                log.info("Job ignored for {}", time.getCron());
            }

        } catch (SchedulerException e) {
            throw new BaseException("Failed to schedule", e);
        } catch (ParseException e) {
            throw new BaseException("Cron expression was not valid", e);
        }

    }

    public void deleteJobs() {
        try {
            scheduler.deleteJob(jobKey);
            log.info("Jobs deleted");
        } catch (SchedulerException e) {
            throw new BaseException("Failed to delete schedule", e);
        }
    }

    @PreDestroy
    private void destroy() {
        try {
            scheduler.shutdown();
        } catch (SchedulerException e) {
            log.error("Failed to stop scheduler", e);
        }
    }

}

