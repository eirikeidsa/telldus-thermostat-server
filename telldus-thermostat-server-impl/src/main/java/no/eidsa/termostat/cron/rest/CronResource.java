package no.eidsa.termostat.cron.rest;

import no.eidsa.termostat.ConvertUtilities;
import no.eidsa.termostat.api.CronsApi;
import no.eidsa.termostat.api.domain.CronTO;
import no.eidsa.termostat.api.domain.CronTemperatureTO;
import no.eidsa.termostat.cron.CronService;
import no.eidsa.termostat.cron.CronTemperature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
public class CronResource implements CronsApi {

    private final CronService cronService;

    @Autowired
    public CronResource(CronService cronService) {
        this.cronService = cronService;
        Assert.notNull(cronService, "cronService should not be null");
    }

    @Override
    public CronTO getCronJobs() throws Exception {
        CronTO to = new CronTO();

        cronService.getCronJobs().forEach(cronTemperature -> to.getCrons().add(ConvertUtilities.convert(cronTemperature)));

        return to;
    }

    @Override
    public void postCronJobs(@Valid CronTO body) throws Exception {
        List<CronTemperature> crons = new ArrayList<>();

        body.getCrons().forEach(cron -> crons.add(ConvertUtilities.convert(cron)));

        cronService.updateCronJobs(crons);

    }


}
