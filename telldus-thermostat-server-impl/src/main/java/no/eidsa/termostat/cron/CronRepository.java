package no.eidsa.termostat.cron;


import org.springframework.data.repository.CrudRepository;

public interface CronRepository extends CrudRepository<CronTemperature, String> {



}
