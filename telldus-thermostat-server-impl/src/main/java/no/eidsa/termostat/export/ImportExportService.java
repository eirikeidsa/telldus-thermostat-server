package no.eidsa.termostat.export;

import no.eidsa.termostat.cron.CronService;
import no.eidsa.termostat.room.RoomService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ImportExportService {

    private final CronService cronService;
    private final RoomService roomService;

    public ImportExportService(CronService cronService, RoomService roomService) {
        this.cronService = cronService;
        this.roomService = roomService;
    }

    public void importDatabase(ImportExport importExport) {
        cronService.deleteAll();
        cronService.updateCronJobs(importExport.getCronTemperatures());

        roomService.deleteAllRooms();
        roomService.saveRooms(importExport.getRooms());
    }

    public ImportExport exportDatabase() {
        return new ImportExport(new ArrayList<>(cronService.getCronJobs()), new ArrayList<>(roomService.getRooms()));
    }

}
