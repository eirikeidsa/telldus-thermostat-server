package no.eidsa.termostat.export;

import lombok.extern.slf4j.Slf4j;
import no.eidsa.termostat.ConvertUtilities;
import no.eidsa.termostat.api.ExportApi;
import no.eidsa.termostat.api.ImportApi;
import no.eidsa.termostat.api.domain.ImportExportTO;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Component
@Slf4j
public class ImportExportResource implements ImportApi, ExportApi {

    private final ImportExportService importExportService;

    public ImportExportResource(ImportExportService importExportService) {
        this.importExportService = importExportService;
    }

    @Override
    public void postDatabase(@Valid ImportExportTO body) throws Exception {
        ImportExport importExport = new ImportExport();

        body.getCronTemperatures().forEach(item -> importExport.getCronTemperatures().add(ConvertUtilities.convert(item)));
        body.getRooms().forEach(item -> importExport.getRooms().add(ConvertUtilities.convert(item)));

        importExportService.importDatabase(importExport);

    }

    @Override
    public ImportExportTO getDatabase() throws Exception {
        ImportExportTO to = new ImportExportTO();

        ImportExport importExport = importExportService.exportDatabase();

        importExport.getRooms().forEach(room -> to.getRooms().add(ConvertUtilities.convert(room)));
        importExport.getCronTemperatures().forEach(cronTemperature -> to.getCronTemperatures().add(ConvertUtilities.convert(cronTemperature)));

        return to;
    }
}
