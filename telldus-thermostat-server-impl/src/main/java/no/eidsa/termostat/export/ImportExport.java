package no.eidsa.termostat.export;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import no.eidsa.termostat.cron.CronTemperature;
import no.eidsa.termostat.room.Room;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImportExport {

    private List<CronTemperature> cronTemperatures = new ArrayList<>();
    private List<Room> rooms = new ArrayList<>();

}
