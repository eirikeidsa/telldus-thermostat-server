package no.eidsa.termostat.metrics;

import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.boot.actuate.health.HealthEndpoint;
import org.springframework.boot.actuate.health.Status;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Health {

    @Bean
    MeterRegistryCustomizer prometheusHealthCheck(HealthEndpoint healthEndpoint) {
        return registry -> registry.gauge("health", healthEndpoint, Health::healthToCode);
    }

    private static int healthToCode(HealthEndpoint ep) {
        Status status = ep.health().getStatus();

        return status.equals(Status.UP) ? 1 : 0;
    }


}
