package no.eidsa.termostat.metrics;

import no.eidsa.termostat.api.PrometheusApi;
import no.eidsa.termostat.room.PrometheusService;
import org.springframework.stereotype.Component;

@Component
public class Prometheus implements PrometheusApi {

    private final PrometheusService prometheusService;

    public Prometheus(PrometheusService prometheusService) {
        this.prometheusService = prometheusService;
    }

    @Override
    public String getRoomPrometheus() throws Exception {
        return prometheusService.generate();

    }
}
