package no.eidsa.termostat.time;

import no.eidsa.termostat.api.TimeApi;
import no.eidsa.termostat.api.domain.TimeTO;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class TimeResource implements TimeApi {
    @Override
    public TimeTO getTimeZone() throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        TimeTO to = new TimeTO();
        ZonedDateTime time = ZonedDateTime.now();
        to.setTimezone(time.getZone().toString());
        to.setTime(time.format(formatter));

        return to;
    }
}
