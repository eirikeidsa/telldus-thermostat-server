package no.eidsa.termostat.telldus;

import no.eidsa.termostat.exception.BaseException;

public class TelldusException extends BaseException {

    public TelldusException(String text) {
        super(text);
    }
}
