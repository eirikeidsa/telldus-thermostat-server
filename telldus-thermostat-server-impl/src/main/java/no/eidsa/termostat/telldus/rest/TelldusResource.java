package no.eidsa.termostat.telldus.rest;

import no.eidsa.termostat.api.TelldusApi;
import no.eidsa.termostat.api.domain.TelldusListTO;
import no.eidsa.termostat.api.domain.TelldusTO;
import no.eidsa.termostat.telldus.DeviceInfo;
import no.eidsa.termostat.telldus.SensorInfo;
import no.eidsa.termostat.telldus.TelldusServiceApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Component
public class TelldusResource implements TelldusApi {

    private final TelldusServiceApi telldusService;

    @Autowired
    public TelldusResource(TelldusServiceApi telldusService) {
        this.telldusService = telldusService;
    }

    @Override
    public TelldusListTO getTelldus() throws Exception {
        List<DeviceInfo> deviceList = telldusService.getDeviceList();
        List<SensorInfo> sensorList = telldusService.getSensorList();

        List<TelldusTO> device = new ArrayList<>();
        List<TelldusTO> sensor = new ArrayList<>();

        deviceList.forEach(deviceInfo -> device.add(createTelldusTO(deviceInfo.getId(), deviceInfo.getName())));
        sensorList.forEach(sensorInfo -> sensor.add(createTelldusTO(sensorInfo.getId(), sensorInfo.getName())));

        TelldusListTO to = new TelldusListTO();

        to.setDevice(device);
        to.setSensor(sensor);

        return to;
    }

    private TelldusTO createTelldusTO(int id, String name) {

        TelldusTO to = new TelldusTO();
        to.setName(name);
        to.setId(id);

        return to;
    }


}
