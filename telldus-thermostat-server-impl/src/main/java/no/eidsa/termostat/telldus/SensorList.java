package no.eidsa.termostat.telldus;

import lombok.Data;

import java.util.List;

@Data
public class SensorList {

    private List<SensorInfo> sensor;

}
