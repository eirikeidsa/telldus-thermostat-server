package no.eidsa.termostat.telldus;

import lombok.Data;

import java.util.List;

@Data
public class SensorInfo {

    private int battery;
    private int id;
    private String model;
    private String name;
    private boolean novalues;
    private String protocol;
    private int sensorId;

    private List<SensorInfoData> data;


}
