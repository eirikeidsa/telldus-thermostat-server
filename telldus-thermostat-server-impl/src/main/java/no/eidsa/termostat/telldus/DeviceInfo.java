package no.eidsa.termostat.telldus;

import lombok.Data;

@Data
public class DeviceInfo {

    private Integer id;
    private Integer methods;
    private String model;
    private String name;
    private String protocol;
    private Integer state;
    private Integer statevalue;
    private String type;

}
