package no.eidsa.termostat.telldus;

import lombok.Data;

@Data
public class DeviceStatus {

    private String status;

}
