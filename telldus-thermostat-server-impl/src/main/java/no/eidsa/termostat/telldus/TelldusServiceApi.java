package no.eidsa.termostat.telldus;

import java.util.List;

public interface TelldusServiceApi {
    Double getCurrentTemperature(String sensorId);

    List<DeviceInfo> getDeviceList();

    List<SensorInfo> getSensorList();

    void turnDeviceOff(String deviceId);

    void turnDeviceOn(String deviceId);
}
