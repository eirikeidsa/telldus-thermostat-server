package no.eidsa.termostat.telldus;

import lombok.Data;

@Data
public class SensorInfoData {

    private String name;
    private Double scale;
    private Double value;

}
