package no.eidsa.termostat.telldus;

import lombok.Data;

import java.util.List;

@Data
public class DeviceList {

    private List<DeviceInfo> device;

}
