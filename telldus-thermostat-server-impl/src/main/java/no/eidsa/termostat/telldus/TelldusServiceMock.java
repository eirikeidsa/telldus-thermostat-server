package no.eidsa.termostat.telldus;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
@ConditionalOnProperty(value = "telldus.mock", havingValue = "true", matchIfMissing = false)
public class TelldusServiceMock implements TelldusServiceApi {
    @Override
    public Double getCurrentTemperature(String sensorId) {
        return 20D;
    }

    @Override
    public List<DeviceInfo> getDeviceList() {
        List<DeviceInfo> list = new ArrayList<>();

        DeviceInfo button = new DeviceInfo();
        button.setId(2);
        button.setType("button");
        button.setName("Ovn");


        list.add(button);

        return list;
    }

    @Override
    public List<SensorInfo> getSensorList() {

        List<SensorInfo> sensors = new ArrayList<>();


        SensorInfo sensor = new SensorInfo();


        sensor.setId(1);
        sensor.setName("Stue");

        SensorInfoData sensorInfoData = new SensorInfoData();
        sensorInfoData.setName("temperature");
        sensorInfoData.setValue(20d);

        List<SensorInfoData> sensorInfoDataList = new ArrayList<>();

        sensorInfoDataList.add(sensorInfoData);

        sensor.setData(sensorInfoDataList);

        sensors.add(sensor);

        return sensors;
    }

    @Override
    public void turnDeviceOff(String deviceId) {

    }

    @Override
    public void turnDeviceOn(String deviceId) {

    }
}
