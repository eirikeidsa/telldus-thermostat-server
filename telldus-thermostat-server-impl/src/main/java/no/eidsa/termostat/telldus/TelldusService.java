package no.eidsa.termostat.telldus;

import lombok.extern.slf4j.Slf4j;
import no.eidsa.termostat.properties.TelldusProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@Slf4j
@ConditionalOnProperty(value = "telldus.mock", havingValue = "false", matchIfMissing = true)
public class TelldusService implements TelldusServiceApi {

    private final TelldusProperties telldusProperties;
    private final RestTemplate restTemplate;

    @Autowired
    public TelldusService(TelldusProperties telldusProperties, RestTemplate restTemplate) {
        Assert.notNull(telldusProperties, "telldusProperties should not be null");
        Assert.notNull(restTemplate, "restTemplate should not be null");
        this.restTemplate = restTemplate;
        this.telldusProperties = telldusProperties;
    }

    @Override
    public Double getCurrentTemperature(String sensorId) {
        String urlForSensorInfo = telldusProperties.getUrl() + "/sensor/info";

        SensorInfo sensorInfo = restTemplate.postForObject(urlForSensorInfo, generateEntity(sensorId), SensorInfo.class);

        Double currentTemp = null;

        for (SensorInfoData sensorInfoData : sensorInfo.getData()) {
            if ("temp".equals(sensorInfoData.getName())) {
                currentTemp = sensorInfoData.getValue();
                break;
            }
        }

        if (currentTemp == null) {
            log.error("Did not find temp in sensor");
        }

        return currentTemp;
    }

    @Override
    public List<DeviceInfo> getDeviceList() {
        String urlForDeviceList = telldusProperties.getUrl() + "/devices/list";
        DeviceList deviceList = restTemplate.postForObject(urlForDeviceList, generateEntity(), DeviceList.class);
        return deviceList.getDevice();
    }

    @Override
    public List<SensorInfo> getSensorList() {
        String urlForDeviceList = telldusProperties.getUrl() + "/sensors/list";
        SensorList sensorInfo = restTemplate.postForObject(urlForDeviceList, generateEntity(), SensorList.class);
        return sensorInfo.getSensor();
    }

    @Override
    public void turnDeviceOff(String deviceId) {
        triggerDevice(deviceId,"turnOff");
    }

    @Override
    public void turnDeviceOn(String deviceId) {
        triggerDevice(deviceId,"turnOn");
    }

    private void triggerDevice(String deviceId, String method) {
        String url = telldusProperties.getUrl() + "/device/" + method;

        DeviceStatus deviceStatus = restTemplate.postForObject(url, generateEntity(deviceId), DeviceStatus.class);

        if ("success".equals(deviceStatus.getStatus())) {
            log.debug("Oven {} {}", deviceId, method);
        } else {
            throw new TelldusException("Failed to turn oven on. Status: " + deviceStatus.getStatus());

        }
    }

    private HttpEntity<MultiValueMap<String, String>> generateEntity(String id) {
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
        formData.add("id", id);

        return new HttpEntity<>(formData, getHttpHeaders());
    }

    private HttpHeaders getHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Bearer " + telldusProperties.getBearerToken());
        return httpHeaders;
    }

    private HttpEntity<MultiValueMap<String, String>> generateEntity() {
        return new HttpEntity<>(new LinkedMultiValueMap<>(), getHttpHeaders());
    }

}
